import { IoImages } from "react-icons/io5";
import { PiArrowArcLeft,  } from "react-icons/pi";
import { SiMarketo } from "react-icons/si";
import { LiaAdversal } from "react-icons/lia";
import { SiYoutubegaming } from "react-icons/si";
import { LiaBusinessTimeSolid } from "react-icons/lia";
import { BiSolidCameraMovie } from "react-icons/bi";

import { FaBookOpenReader } from "react-icons/fa6";



 const  category =[
    {
        contestType:"Image Design Contests",
        icon:IoImages
    },
    {
        contestType:"Article Writing Challenge",
        icon:PiArrowArcLeft
    },
    {
        contestType:"Marketing Strategy Competition",
        icon:SiMarketo   
    },
    {
        contestType:"Digital Advertisement Contest",
        icon:LiaAdversal
    },
    {
        contestType:"Gaming Review Contest",
        icon:SiYoutubegaming
    },
    {
        contestType:"Book Review Competition",
        icon:FaBookOpenReader
    },
    {
        contestType:"Business Idea Contest",
        icon:LiaBusinessTimeSolid
    },
    {
        contestType:"Movie Review Competition",
        icon:BiSolidCameraMovie
    },
]


export default category