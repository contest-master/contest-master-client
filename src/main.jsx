import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {  RouterProvider } from 'react-router-dom'
import route from './AllPages&Component/Route/Route'
import AuthProvider from './AllPages&Component/Provider/AuthProvider'
import { HelmetProvider } from 'react-helmet-async'
import { Toaster } from 'react-hot-toast'
import '@animxyz/core'

import {
 
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query'
import ContestProvider from './AllPages&Component/Provider/ContestProvider'

const queryClient = new QueryClient()





ReactDOM.createRoot(document.getElementById('root')).render(



  <React.StrictMode>
 <QueryClientProvider client={queryClient}>
  <AuthProvider>
   <ContestProvider>
   <HelmetProvider>
    <div className='max-w-7xl   mx-auto'>
    <RouterProvider router={route}/>
    </div>
    <Toaster/>
    </HelmetProvider>
   </ContestProvider>
   </AuthProvider>
    </QueryClientProvider> 
  </React.StrictMode>,

)
