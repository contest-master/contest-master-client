import { createContext, useContext, useState } from "react";
export const ContestContext =  createContext(null)


const ContestProvider = ({children}) => {
    const [selectContestType,setSelectContestType] = useState(null)
        return (
        <ContestContext.Provider value={{selectContestType,setSelectContestType}}>
            {children}
        </ContestContext.Provider>
    );
};



export default ContestProvider;