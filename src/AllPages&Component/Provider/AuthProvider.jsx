import  { createContext, useEffect, useState } from 'react';
import { GoogleAuthProvider, createUserWithEmailAndPassword, getAuth, onAuthStateChanged, signInWithEmailAndPassword, signInWithPopup, signOut, updateProfile } from "firebase/auth";
import app from '../../../firebase.config';
import useAxios from '../Hooks/useAxios';



export const AuthInfo = createContext(null)
const AuthProvider = ({children}) => {
    const [loading,setLoading] = useState(true)
    const [user,setUser]=useState(null)
    const auth = getAuth(app);
    const googleAuth = new GoogleAuthProvider()
    const axiosOpen = useAxios()

    const createUser = (email,password)=>{
        setLoading(true)
        return createUserWithEmailAndPassword(auth,email,password)
    }

    const googleLogin = ()=>{
        setLoading(true)
        return signInWithPopup(auth,googleAuth)

    }

    const login=(email,password)=>{
        setLoading(true)
        return signInWithEmailAndPassword(auth,email,password)
    }

    const updateUser=(name,imageUrl)=>{
        return updateProfile(auth.currentUser,{
            displayName:name , photoURL: imageUrl

        })

    }

    const logOut = () =>{
        setLoading(true);
        return signOut(auth).finally(() => {
            setUser(null);
            setLoading(false);
        });
    }


    useEffect(()=>{
        const unsubscribe = onAuthStateChanged(auth,async(currentUser)=>{
            setUser(currentUser)
            if(currentUser){
                const userInfo = {email:currentUser?.email}
              try {
                const res = await axiosOpen.post('/jwt',userInfo)
                const token={token:res.data}
                if(token){
                    localStorage.setItem('token',res.data)
                }



                console.log(token,'--------->');
              } catch (error) {
                console.log(error);
              }
            }
            setLoading(false)
        })
        return ()=>{
            return unsubscribe()
        }
    },[])





    
    const submit={
        createUser,
        login,
        googleLogin,
        updateUser,
        logOut,
        loading,
        setLoading,
        user
    }
    return (
       <AuthInfo.Provider value = {submit}>
        {children}
       </AuthInfo.Provider>
    );
};

export default AuthProvider;