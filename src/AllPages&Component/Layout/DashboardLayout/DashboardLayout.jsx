import { FaBars, FaCheck, FaCross, FaHome, FaRegAddressBook, FaShoppingCart, FaUser } from "react-icons/fa";
import { FaBookOpenReader, FaBurger, FaMagnifyingGlass, FaX } from "react-icons/fa6";
import { Link, NavLink, Outlet } from "react-router-dom";
import useUserParticipatedData from "../../Hooks/useUserParticipatedData";
import {useState } from "react";
import ThemeToggle from "../../Utils/ThemeToggle";
import useContestResult from "../../Hooks/useContestResult";
import useAuth from "../../Hooks/useAuth";
import useContest from "../../Hooks/useContest";
import useAdmin from "../../Hooks/useAdmin/useAdmin";
import useDbUserData from "../../Hooks/useDbUserData";



const DashboardLayout = () => {
    const [nameLinkShow,setNameLinkShow] = useState(false)
    const {part,join,refetch} =  useUserParticipatedData()
    const {contestResult,contestResultFetch,myContestParticipated} = useContestResult()
    const {setSelectContestType} = useContest()
    const {dbUser} = useDbUserData()

    const [toggle,setToggle]=useState(false)

    const {user} = useAuth()
    const email = user?.email

    const currentUserIsAdmin = dbUser?.find(d=>d.email===email && d.role==="Admin")
    const moderateUser = dbUser?.find(d=>d.email===email && d.role==="Moderator")
    const generalUser = dbUser?.find(d=>d.email===email && d.role==="User")

//admin---

    





    const id = Array.from(new Set(contestResult?.map(d=>d.contestSubmitId)))  
    const newNumber = join?.filter(d=> !id.includes(d._id))     // join contest length

    const myContestNumber = contestResult?.filter(d=>d.participateEmail===email ) // my contest result length

    const newContestType = Array.from(new Set(myContestParticipated?.filter(d=>d.result==="pending").map(d=>d?.contestName)))

     const handleNameWiseData=(d)=>{
        
        if(nameLinkShow===true){
            setSelectContestType(d)
        }
        if(nameLinkShow===false){
            setSelectContestType(null)
        }
        
        }
    // console.log(nameLinkShow);


    contestResultFetch()
    refetch()
    const dashNav = <>
    {
        generalUser && <>
         <li><NavLink end exact to='/dashboard' className={`flex gap-2 dashNav`}><FaUser/>  
             User Profile
          </NavLink></li>

          <li>
        <NavLink to='cart' className={`flex gap-2 dashNav`}><FaShoppingCart/> Cart {`(${part?.length})`} </NavLink>
         </li>

         
    <li><NavLink  to='join-contest'  className="flex gap-2 dashNav"><FaCheck/> Join Contest ({newNumber?.length}) </NavLink></li>

    
    
    <li><NavLink to='my-contest-result'  className="flex gap-2 dashNav"><FaCheck/> My Contest Result ({myContestNumber?.length}) </NavLink></li>

        </>
    }
   
 

 
    {
        currentUserIsAdmin  && 
    <>
      <li><NavLink end exact to='/dashboard' className={`flex gap-2 dashNav`}><FaUser/>  
             User Profile
          </NavLink></li>

    <li><NavLink to='add-contest'  className="flex gap-2 dashNav"><FaRegAddressBook/> Add Contest</NavLink></li>
      
    <li><NavLink to='my-contest'  className="flex gap-2 dashNav" ><FaMagnifyingGlass/> My Contest</NavLink></li>
    <li><NavLink to='approve-participates' className="flex items-center gap-2 dashNav"><FaCheck/> Confirm Participates</NavLink></li>


    <li className="relative" onClick={()=>{setNameLinkShow(!nameLinkShow)}}><NavLink to='submitted-contest' className="flex items-center gap-2 pt-10  dashNav"><FaBookOpenReader/> Submitted Result({myContestParticipated?.filter(d=>d.result==="pending").length}) <br />
    </NavLink></li>

    <li className={`absolute text-[10px] active:text-red-500     flex flex-col bg-[#5b178d] 
        transition transform duration-700 ${!nameLinkShow?"translate-y-60 hidden ":'block translate-y-0'}`}>

        {   newContestType?.map((d,idx)=>(<button className="my-1 text-sm"  key={idx} onClick={()=>handleNameWiseData(d)}>{d}</button>         ))}
    </li>
  

     <li><NavLink to='all-users' className="flex items-center  gap-2 dashNav"><FaUser/> All Users</NavLink></li>
    </>
  }
    <hr />



    <li><NavLink to='/'  className="flex gap-2"><FaHome/> Home</NavLink></li>


    </>

    return (
        <div className="relative  ">
            <div className="  md:flex justify-center h-[500px]">
                {/* Big screen dashboard nav*/}
              <div className="purple-bg hidden md:block text-white  w-1/4 h-screen">
              <div className="  md:flex justify-center font-poppins ">
                <ul className="mt-8 space-y-3 ">
                    {dashNav}
                <ThemeToggle/>
                </ul>
                </div>
              </div>
                {/* mini screen dashboard nav */}
                <div className={`md:hidden absolute purple-bg z-50 text-white   w-2/3  h-screen flex justify-center  font-poppins ${toggle?"-translate-x-96 ":'translate-x-0'} transition duration-700`} >
                <ul className="mt-8 md:space-y-3 ">
                    {dashNav}
                <ThemeToggle/>
                </ul>
                </div>
                <div className="absolute right-0 md:hidden">
                   <button onClick={()=>setToggle(!toggle)}> 
                   {
                    toggle?<FaBars className="text-2xl purpleText"/>:<FaX className="text-2xl purpleText"/>

                   }
                    
                    </button>
                </div>

                <div className=" w-full md:p-2 md:mx-2  "><Outlet/></div>
            </div>
        </div>
    );
};

export default DashboardLayout;