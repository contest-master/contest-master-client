import {  useQuery } from "@tanstack/react-query";
import useAxiosSecure from "./useAxiosSecure";
import useAuth from "./useAuth";

const usePaymentCollection = () => {
    const axiosSecure = useAxiosSecure()
    const {user} = useAuth()
    const email = user?.email
    const {data:paymentCollection,isLoading:paymentCollectionLoading,error:paymentCollectionError ,refetch} = useQuery({
        queryKey:['paymentCollection'],
        queryFn:async()=>{
            const {data} = await axiosSecure.get(`/payment-collection/${email}`)
            console.log(data);
            return data
        }
    })
    return {
        paymentCollection,
        paymentCollectionLoading,
        paymentCollectionError,
        refetch
    }
};

export default usePaymentCollection;