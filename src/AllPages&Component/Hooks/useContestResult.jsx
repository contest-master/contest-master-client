import { useQuery } from '@tanstack/react-query';
import useAxiosSecure from './useAxiosSecure';
import useAuth from './useAuth';


const useContestResult = () => {
    const axiosSecure = useAxiosSecure()
    const {user} = useAuth()
    const email = user?.email


    const {data:contestResult,isLoading:contestResultLoading,error:contestResultError,refetch:contestResultFetch} = useQuery({
        queryKey:["contestResult"],
        queryFn:async()=>{
            const {data} = await axiosSecure.get('/contest-result')
            return data
        }
    })

// console.log(contestResult?.filter(d=>d.publisher.email===email ));
    const myContestParticipated = contestResult?.filter(d=>d?.publisher?.email===email)

    return {contestResult,myContestParticipated,contestResultLoading,contestResultError,contestResultFetch}
};

export default useContestResult;