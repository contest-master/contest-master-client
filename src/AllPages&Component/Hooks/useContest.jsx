import  { useContext } from 'react';
import { ContestContext } from '../Provider/ContestProvider';

const useContest = () => {
    const all = useContext(ContestContext)
    return all
}
;
export default useContest;