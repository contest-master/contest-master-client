import { useMutation } from '@tanstack/react-query';
import useAxiosSecure from './useAxiosSecure';

const useMutationPost = () => {
    const axiosSecure = useAxiosSecure()
    return useMutation(async(url,data)=>{
      const response = await axiosSecure.post(url,data)
      return response.data
    })
    
     
 
};

export default useMutationPost;