import { useQuery } from "@tanstack/react-query";
import useAxiosSecure from "./useAxiosSecure";
import useAuth from "./useAuth";


const useDbUserData = () => {
    const {user} = useAuth()
    const name = user?.displayName
    const email = user?.email
    const imageUrl = user?.photoURL
    const role = 'user'
    const axiosSecure = useAxiosSecure()


    const {data:dbUser,loading:dbUserLoading,error:dbUserError,refetch:dbUserRefetch} = useQuery({
        queryKey:['userDB'],
        queryFn:async()=>{
            const {data} = await axiosSecure('/db-user')
            return data
        }
    })

    const  currentUser= dbUser?.find(data=>data.email===email)
    if(!currentUser){
        const userProfile ={
            email,name,imageUrl,role
        }
        try {
            const {data} = axiosSecure.post('/user-data',userProfile)
            console.log(data);
        } catch (error) {
            console.log(error);
        }
    }



    return {dbUser,currentUser,dbUserLoading,dbUserError,dbUserRefetch}
  
};


export default useDbUserData;