import axios from "axios";
import useAuth from "./useAuth";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";





const axiosSecure = axios.create({
    baseURL:'http://localhost:4000',

})

let intercepAdded = false
const useAxiosSecure = () => {
    const {logOut} = useAuth()
    const navigate = useNavigate()
    useEffect(()=>{
        if(!intercepAdded){
        
            axiosSecure.interceptors.request.use(function(config){
                const token = localStorage.getItem('token')
                config.headers.authorization =`token ${token}`;
                return config;
            },function(error){
                return Promise.reject(error)
            })
        
            axiosSecure.interceptors.response.use(function(response){
                return response
            }, async(error)=>{
                const status = error.response.status
                if(status===401){
                    await logOut()
                    navigate('/login')
                }
        
                Promise.reject(error)
                return Promise.reject(error)
        
            } )
            intercepAdded=true
            }
    },[logOut,navigate])

  
    return axiosSecure
};

export default useAxiosSecure;