import React from 'react';
import useAuth from './useAuth';
import { useQuery } from '@tanstack/react-query';
import useAxiosSecure from './useAxiosSecure';

const useUserParticipatedData = () => {
    const {user} = useAuth()
    const email = user?.email
    const axiosSecure = useAxiosSecure()
    const {data:participatedData,error,isLoading,refetch} = useQuery({
        queryKey:['participated User',email],
        queryFn:async()=>{
            const {data} = await axiosSecure.get(`/participated-user/${email}`)
            return data
        }
    })
    const part = participatedData?.filter((data)=>data?.paymentApproval!=="Approve")
    const join = participatedData?.filter((data)=>data?.paymentApproval==="Approve")

   return {part,join,participatedData,error,isLoading,refetch}
};

export default useUserParticipatedData;