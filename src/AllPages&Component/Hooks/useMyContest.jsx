import { useQuery } from "@tanstack/react-query";
import useAxiosSecure from "./useAxiosSecure";

const useMyContest = ({email}) => {
    const axiosSecure = useAxiosSecure()
    const {data:myContest,isLoading,error,refetch} =  useQuery({
        queryKey:["contest",email],
        queryFn:async()=>{
            const {data} = await axiosSecure(`/contest/${email}`)
            return data
        }
    })

    return{myContest,isLoading,error,refetch}
}
export default useMyContest;