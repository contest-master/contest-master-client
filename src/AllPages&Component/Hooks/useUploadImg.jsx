import axios from 'axios';
import React, { useState } from 'react';

const useUploadImg = () => {
    const [error, setError] = useState();
    const [imageUrl, setImageUrl] = useState(null);
    const[imageUploadLoading,setImageUploadLoading]=useState(false)

   const uploadImage=async(imageFile)=>{
    setImageUploadLoading(true)
    
    const form = new FormData
    form.append('image',imageFile)
    const res = await axios.post(`https://api.imgbb.com/1/upload?key=72cf15898570da3fa0361cb4721a6776`,form)
    console.log(res.data.data.display_url);
    setImageUploadLoading(false)
    setImageUrl(res.data.data.display_url)

   }

    return { imageUrl,uploadImage,  error,imageUploadLoading };
};

export default useUploadImg;