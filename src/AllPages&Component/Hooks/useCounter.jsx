import React from 'react';
import useAxiosSecure from './useAxiosSecure';
import { useQuery } from '@tanstack/react-query';

const useCounter = () => {
    const axiosSecure = useAxiosSecure()
    const {data:count,isLoading:counterLoading,error:counterError}=useQuery({
        queryKey:['countValue'],
        queryFn:async()=>{
            const res = await axiosSecure.get('/counter')
            return res.data
        }
    })  

    return {count,counterLoading,counterError}
};

export default useCounter;