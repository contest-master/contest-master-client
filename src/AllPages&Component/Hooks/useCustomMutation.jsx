import { useMutation, useQueryClient } from '@tanstack/react-query';
import React from 'react';

const useCustomMutation = (mutationFn,options={})=>{
    const queryClient = useQueryClient()
    
    return useMutation(mutationFn,{
        ...options,
        onSuccess:(data,variables,context)=>{
            if(options.onSuccess){
                options.onSuccess(data,variables,context)
            }   
            queryClient.invalidateQueries('contest');
        },
        onError: (error, variables, context) => {
            // Call the user-defined onError handler if provided
            if (options.onError) {
              options.onError(error, variables, context);
            }
        
    }
     })
}
export default useCustomMutation