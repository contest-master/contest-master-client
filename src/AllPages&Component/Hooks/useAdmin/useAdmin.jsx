import { useQuery } from "@tanstack/react-query";
import useAuth from "../useAuth";
import useAxiosSecure from "../useAxiosSecure";


const useAdmin = () => {
    const {user} = useAuth()
    const axiosSecure  = useAxiosSecure()
    
    const {data:isAdmin} = useQuery({
        queryKey:['isAdmin',user?.email],
        queryFn:async()=>{
            if (!user?.email) {
                throw new Error('User email not available');
                }
                const res = await axiosSecure.get(`/user/admin/${user?.email}`);
                console.log(res);
            return res?.data?.admin;
        }
    })
return [isAdmin]
};

export default useAdmin;