import useAxios from './useAxios';
import { useQuery } from '@tanstack/react-query';


const useAllContest = () => {
    const axiosOpen = useAxios();
    const { data: allContest, isLoading } = useQuery({
      queryKey: ["todos"],
      queryFn: async () => {
        const { data } = await axiosOpen.get("/contest");
        return data;
      },
    });

    return{allContest,isLoading}
};

export default useAllContest;