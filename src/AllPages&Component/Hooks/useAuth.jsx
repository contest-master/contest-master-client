import { useContext } from 'react';
import { AuthInfo } from '../Provider/AuthProvider';

const useAuth = () => {
    const all = useContext(AuthInfo)
    return all
};

export default useAuth;