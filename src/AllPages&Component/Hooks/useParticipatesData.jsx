import React from 'react';
import useAxiosSecure from './useAxiosSecure';
import useAuth from './useAuth';
import { useQuery } from '@tanstack/react-query';

const useParticipatesData = () => {
    const {user} = useAuth()
    const email = user?.email
    const axiosSecure = useAxiosSecure()


    const {data:participates,isLoading:participateDataLoading,refetch} = useQuery({
        queryKey:['participateData',],
        queryFn:async()=>{
            const {data}=await axiosSecure.get('/participates-data')
            return data
        }
    })
    const myContestParticipates = participates?.find(p=>p.email===email)



console.log(participates);


    return{participates,participateDataLoading,myContestParticipates}
};

export default useParticipatesData;