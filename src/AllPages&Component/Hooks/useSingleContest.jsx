import useAxiosSecure from './useAxiosSecure';
import { useQuery } from '@tanstack/react-query';

const useSingleContest = (id) => {
    const axiosSecure=useAxiosSecure()
    return useQuery({
        queryKey:['singleContest',id],
        queryFn:async()=>{
            const {data}=await axiosSecure.get(`/contestDetails/${id}`)
            return data
        }
    })
};

export default useSingleContest;