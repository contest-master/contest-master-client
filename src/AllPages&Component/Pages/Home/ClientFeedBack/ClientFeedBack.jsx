import TitleAndSubtitle from "../../../Components/utils/TitleAndSubtitle";
import useDbUserData from "../../../Hooks/useDbUserData";
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import icon from '/arrow.png'
import {Autoplay} from 'swiper/modules';


const ClientFeedBack = () => {

    const {dbUser} = useDbUserData()
    const realClient = dbUser?.filter(d=>d?.role!=="Admin")
    
    
    return (
        <div>
    <TitleAndSubtitle title={"what Say Our Participates"} subTitle={"We are alway concern about out participates how feel about us."}></TitleAndSubtitle>

<div className="w-full  md:w-3/5 px-5 mx-auto mt-11">
<Swiper
        slidesPerView={1}
        spaceBetween={25}
        loop={true}
        autoplay={{
            delay: 1500,
            disableOnInteraction: false,
          }}
          modules={[Autoplay]}

        className="mySwiper"
      >
        
        <div className="">
        {
    realClient?.map((d,idx)=>(
        <SwiperSlide key={idx} className="mx-auto w-full text-center">
        <div className="w-full shadowF  mx-auto border-2 rounded-xl border-purple-700 p-5    ">
        <img src={icon} alt="" className="w-28 mx-auto" />
        <p>
         Appropriately repurpose dynamic meta-services after worldwide web services. Interactively foster compelling web-readiness for sticky manufactured products.  
         </p>

        <div>
        <img src={d.imageUrl} alt="" className="h-14 my-5 mx-auto"/>
        
         <div>
             <p>{d.name}</p>
             <p>{d.email}</p>
         </div>
        </div>
        </div>
     </SwiperSlide>




        // <div key={d.id} className=" mx-auto bg-base-100 shadow-xl font-poppins">
        // </div>
    ))
}
        </div>
</Swiper>
</div>
        </div>
    );
};

export default ClientFeedBack;