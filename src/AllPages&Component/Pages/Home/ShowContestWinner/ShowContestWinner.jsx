import 'keen-slider/keen-slider.min.css'
import useContestResult from '../../../Hooks/useContestResult';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import { Autoplay } from 'swiper/modules';
import TitleAndSubtitle from '../../../Components/utils/TitleAndSubtitle';


const ShowContestWinner = () => {

    const {contestResult,contestResultLoading} = useContestResult()
    const groupResult = {}

    contestResult?.forEach((results)=>{
        const {contestName} =  results
        if(!groupResult[contestName]){
            groupResult[contestName] =  []
        }
        groupResult[contestName].push(results)
        return groupResult
    })

    const da =Object.entries(groupResult).map(([contestName,results])=>(contestName,results))


// console.log(da);
   
    






    return (
        <div className='-mt-10 md:mt-0'>
            <div>
                <TitleAndSubtitle 
                title={'Checkout Contest Winners'}
                subTitle={'This time you can place here ,then Why so late'}
                />
            </div>
            <div>
            <Swiper
        spaceBetween={30}
        centeredSlides={true}
        loop={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
     
        modules={[Autoplay]}
        className="mySwiper"
      >
       
        {
        Object.entries(groupResult)?.map(([contestName,results])=>(
            <SwiperSlide key={contestName} className=''>
                       <div key={contestName} className=" w-full md:w-2/3 rounded-lg border-2 border-x-blue-600 shadowOne p-3 mx-auto bg-gradient-to-r from-purple-500 to-black font-poppins">
                       <div className='text-white text-2xl md:text-3xl text-center mb-5 border-4 border-white py-2 rounded-xl'> Top Three Winners According to Contest </div>
                <h1 className="text-white text-center text-xl mb-6">Contest Name : {contestName}</h1>
               <div className='grid grid-rows-2 grid-cols-2 justify-items-center'>

               {
                    results.sort((a,b)=>a.result-b.result).slice(0,3).
                    map((d,idx)=>{
                        if(idx===0){
                          return  <div key={idx} className='row-span-1 border  border-white py-2 rounded-2xl bg-purple-900  px-9 col-span-2 justify-items-center text-center mb-6 md:mb-0'>
                                <img  src={d.participateImage} className='w-28 rounded-xl h-28' alt="" />
                                <h1 className='text-white text-md font-semibold mt-2'> {d.participateName}</h1>
                                <h1 className='text-white text-md font-semibold'>Result : {d.result} </h1>
                            </div>
                        }else{
                            return <div key={idx} className='row-span-2   col-span-1  text-center justify-items-center'>
                            <img  src={d.participateImage} className='w-28 h-28 rounded-xl' alt="" />
                            <h1 className='text-white text-md font-semibold'> {d.participateName}</h1>
                            <h1 className='text-white text-md font-semibold'>Result : {d.result} </h1>
                        </div>
                        }
                    })
                }
               </div>
            </div>
            </SwiperSlide>






     
        ))
     }
  
      </Swiper>

            
            </div>
        </div>
    );
};

export default ShowContestWinner;