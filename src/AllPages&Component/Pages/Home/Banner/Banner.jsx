import bannerImg1 from '../../../../assets/banner/horse2.jpg'
import { useKeenSlider } from "keen-slider/react"
import "keen-slider/keen-slider.min.css"
import { FaArrowCircleRight } from 'react-icons/fa';


const Banner = () => {
    const [sliderRef] = useKeenSlider(
        {loop: true},
        [
            (slider) => {
              let timeout
              let mouseOver = false
              function clearNextTimeout() {
                clearTimeout(timeout)
              }
              function nextTimeout() {
                clearTimeout(timeout)
                if (mouseOver) return
                timeout = setTimeout(() => {
                  slider.next()
                }, 2000)
              }
              slider.on("created", () => {
                slider.container.addEventListener("mouseover", () => {
                  mouseOver = true
                  clearNextTimeout()
                })
                slider.container.addEventListener("mouseout", () => {
                  mouseOver = false
                  nextTimeout()
                })
                nextTimeout()
              })
              slider.on("dragStarted", clearNextTimeout)
              slider.on("animationEnded", nextTimeout)
              slider.on("updated", nextTimeout)
            },
          ]
    )















    return (
        <div style={{backgroundImage:`url(${bannerImg1})`}} className='w-full rounded-lg h-[50vh] md:h-[calc(100vh-80px)] bg-gradient-to-r from-indigo-500  bg-cover bg-center' >
            <div className='h-full w-full bg-gradient-to-r from-[#5d2167] rounded-lg'>
                <div className='w-full  md:w-1/2 h-full flex items-center justify-center '>

                <div ref={sliderRef} className="keen-slider text-white font-poppins px-5">

                <div className="keen-slider__slide number-slide1 text-left md:text-center ">
                <h1 className='text-3xl md:text-5xl font-semibold'>Best Contest <br className='md:hidden' /> Platform</h1>
                <p className='text-md md:text-xl mt-2'>  Participate with minimal entry fees</p>
                <p className='text-md md:text-xl mt-2'>Win attractive prizes</p>

                </div>


                <div className="keen-slider__slide number-slide2 ">
                    <h1 className='text-5xl'>Best contest winner</h1>
                    <p className="text-sm mt-2 flex items-center gap-2"><strong><FaArrowCircleRight/></strong> Increased Visibility and Recognition.</p>  

                    <p className="text-sm mt-2 flex items-center gap-2"><strong><FaArrowCircleRight/></strong> Exclusive Prizes and Rewards</p>
                    <p className="text-sm mt-2 flex items-center gap-2"><strong><FaArrowCircleRight/></strong> Networking Opportunities</p>




                    
                    </div>

                <div className="keen-slider__slide number-slide2 text-5xl">
                <h1 className="text-3xl md:text-5xl font-semibold ">Why Join with Us?</h1>
                <div className="benefits mt-4 ">
                    <p className="text-sm mt-2 flex items-center gap-2"><strong> <FaArrowCircleRight/> </strong> Join contests with just a few clicks.</p>

                    <p className="text-sm mt-2 flex items-center gap-2"><strong><FaArrowCircleRight/></strong> New opportunities to win every week.</p>
                    <p className="text-sm mt-2 flex items-center gap-2"><strong><FaArrowCircleRight/></strong> Connect with other participants and share your experiences.</p>
                    <p className="text-sm mt-2 flex items-center gap-2"><strong><FaArrowCircleRight/></strong> Win amazing prizes regularly.</p>
                </div>
                </div>
             
                </div>
                </div>
            </div>
        </div>
    );
};

export default Banner;