import { category } from "../../../Pages/Home/ContestType/category.js";
import { Tab, TabGroup, TabList, TabPanel, TabPanels } from "@headlessui/react";

import { Link } from "react-router-dom";
import { useState } from "react";
import useAllContest from "../../../Hooks/useAllContest.jsx";
import TitleAndSubtitle from "../../../Components/utils/TitleAndSubtitle.jsx";

const ContestType = () => {
const {allContest,isLoading} = useAllContest()

  const [categoryWise , setCategoryWise] = useState(null)
  // console.log(categoryWise);



  const isCategory =categoryWise && categoryWise!=="ALL Contest"?allContest.filter(d=>d.contestType===categoryWise) :allContest

 
  return (
   <div>
     <div className="w-full md:pt-11 mx-auto ">
   <div className="my-10">
   <TitleAndSubtitle title={'Choose and Select Your Preferable Contest'} subTitle={'After win you will more than 2x profit return'} />
   </div>
      <TabGroup className={"w-full mx-auto text-center "}>
        <TabList>
          {category?.map((d, idx) => (
            <Tab key={idx} className={"my-3"}>
              <div className=" px-5 mx-auto text-center">
                <d.icon className="mx-auto text-purple-700 font-semibold text-xl" />
                <span onClick={() => setCategoryWise(d.contestType)} className="font-semibold">
                  {d.contestType}
                </span>
                {/* {d.icon} */}
              </div>
            </Tab>
          ))}
        </TabList>
        {
          isCategory?.length <=0?<>
          <div className="w-full text-center text-purple-900 font-semibold py-10">
            <h1>This Category has not found any Contest</h1>
            <h1 className="text-3xl">Please select another category</h1>
          </div>
          </>
          :
         <div className="">
           <div className="md:grid grid-cols-2  md:gap-5 ">
          {  isCategory?.slice(0,8).map((data, idx) => (
            <TabPanels key={idx}>
              <div className="flex flex-col px-1  md:px-0 mt-6 md:flex-row w-full border border-black rounded-lg shadow ">
                <div className="md:h-56">
                  <img src={data.image} alt="" className="h-full rounded-lg"/>
                </div>
                <div className="max-w-full text-left p-6 bg-white">
                  <p className=" font-normal purpleText dark:text-gray-400">
                    {" "}
                    {data.contestType}{" "}
                  </p>

                  <a href="#">
                    <h5 className=" text-2xl purpleText font-semibold tracking-tight text-black">
                      {data.contestName}
                    </h5>
                  </a>
                  <p className="mb-3 font-normal text-gray-500 dark:text-gray-400">
                    {data.contestDescription}{" "}
                  </p>
                  <p className="mb-3 font-normal text-gray-500 dark:text-gray-400">
                    {" "}
                    Participate price:{data.price} ${" "}
                  </p>

                  <Link
                    to={`/contestDetails/${data._id}`}
                    className="inline-flex font-medium items-center text-blue-600 hover:underline"
                  >
                    See details
                    <svg
                      className="w-3 h-3 ms-2.5 rtl:rotate-[270deg]"
                      aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 18 18"
                    >
                      <path
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M15 11v4.833A1.166 1.166 0 0 1 13.833 17H2.167A1.167 1.167 0 0 1 1 15.833V4.167A1.166 1.166 0 0 1 2.167 3h4.618m4.447-2H17v5.768M9.111 8.889l7.778-7.778"
                      />
                    </svg>
                  </Link>
                </div>
                
              </div>
            
            </TabPanels>
            
          ))
        }
          { categoryWise==="ALL Contest" && allContest &&(
            <Link to={'show-all'}>   <button  className="gradient-button w-full">
            See more</button></Link>
            )
              }
      
       
     
        </div>
       
         </div>
        }

     
      </TabGroup>
    </div>
    
           
   </div>
  );
};

export default ContestType;
