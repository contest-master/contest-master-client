
import { Link, useNavigate, useParams } from 'react-router-dom';
import useSingleContest from '../../../Hooks/useSingleContest';
import useAuth from '../../../Hooks/useAuth';
import toast from 'react-hot-toast';
import useAxios from '../../../Hooks/useAxios';
import { useMutation } from '@tanstack/react-query';
import { useState } from 'react';
import UseHelmet from '../../../Components/UseHelmet/UseHelmet';
import useDbUserData from '../../../Hooks/useDbUserData';
import useAllContest from '../../../Hooks/useAllContest';

const ContestSingleCard = () => {
    const {user} = useAuth()
    const params = useParams()
    const id = params.id
    const {data:singleContest,isLoading} = useSingleContest(id)
    const axiosOpen = useAxios()
    const [loading,setLoading] = useState(null)
    const navigate = useNavigate()
    const publisher =singleContest?.publisher 
    const {currentUser,dbUser} =  useDbUserData()
    const {allContest} = useAllContest()

    const publisheMail =singleContest?.publisher?.email
    const thisUser = currentUser?.email
    

    
    const contestId = singleContest?._id;
    const  contestName = singleContest?.contestName;
    const contestPrice = singleContest?.price;
    const participateName = user?.displayName;
    const participateEmail = user?.email;
    const participateImage = user?.photoURL;
    const ContestWinningPrice = singleContest?.prize  
    const contestType = singleContest?.contestType
    const publishDate = singleContest?.publishDate
    const endDate = singleContest?.endDate
    const image = singleContest?.image;
    const taskSubmissionInstructions=singleContest?.taskSubmissionInstructions
    
    console.log(singleContest);

    const {mutateAsync} = useMutation({
      mutationFn:async(myCart)=>{
        const {data} = await axiosOpen.post('/participate-collection',myCart)
        console.log(data);
        return data
      },
      onSuccess:()=>{
        setLoading(false)
        //too--do swal
        navigate('/')
        return toast.success('Contest added in cart')
    },
    onError:(error)=>{
        setLoading(false)
        console.log(error);
        return toast.error(error.response.data.message)
    }
    })
    
    const handleAddToCart =async () =>{
      const data = {
        contestId,contestName,contestPrice,participateName,participateEmail,participateImage,ContestWinningPrice,contestType  ,publishDate,endDate,image,publisher, taskSubmissionInstructions
      }
      if(thisUser===publisheMail || publisheMail==="Admin"||publisheMail=== "Moderator"){
        return toast.error('You have not permission to participate');
      }
      



      try {
         const {result} = mutateAsync(data)
         console.log(result,'data post');
      } catch (error) {
        console.log(error);
      }
    }
    return (
        <div>
          <UseHelmet title='Add to Cart'/>
            
            <div
  className="relative grid h-screen w-full max-w-7xl flex-col items-end justify-center overflow-hidden rounded-xl bg-white bg-clip-border text-center text-gray-700">

  <div
    className={`absolute inset-0 m-0 h-full w-full overflow-hidden rounded-none bg-transparent purple-bg bg-cover bg-clip-border bg-center text-gray-700 shadow-none font-poppins`}>
              <p className='text-white text-md mt-2 '>Largest Contest Management Website</p>
              <h1 className='text-white text-4xl'>Contest Master</h1>
    <div className="absolute inset-0 w-full h-full to-bg-black-10 bg-gradient-to-t from-black/80 via-black/50"></div>
  </div>
  <div className="absolute w-full flex justify-center items-center h-screen p-6 px-6  md:px-12">
    
        
<div href="#" className="flex flex-col items-center justify-center bg-white border border-gray-200 rounded-lg shadow md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
    <div className="flex flex-col justify-between p-4 leading-normal">
        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{singleContest?.contestName}</h5>
   
        <p className="mb-3 font-normal text-left  text-white ">Task Instruction : {singleContest?.taskSubmissionInstructions} $</p>
        <p className="mb-3 font-normal text-left  text-white ">Price : {singleContest?.price} $</p>
        <p className="mb-3 font-normal text-left  text-white ">Winning Price : {singleContest?.prize} $</p>
        <p className="mb-3 font-normal text-left  text-white ">Publish Date : {singleContest?.publishDate} </p>
        <p className="mb-3 font-normal text-left  text-white ">End Date : {singleContest?.endDate} </p>
        <p className="mb-3 font-normal text-left  text-white ">Category : {singleContest?.contestType} </p>



        <button onClick={handleAddToCart} className='btn btn-primary purpleLinear'>Add to Cart</button>
    </div>
</div>




  </div>
</div>  

        </div>
    );
};

export default ContestSingleCard;