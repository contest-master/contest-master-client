import { Helmet } from "react-helmet";
import useAuth from "../../../Hooks/useAuth";
import Banner from "../Banner/Banner";
import Navbar from "../Navbar/Navbar";
import ContestType from "../ContestType/ContestType";
import ShowContestWinner from "../ShowContestWinner/ShowContestWinner";
import MiddleBanner from "../MiddleBanner/MiddleBanner";
import AllTimeBestContest from "../AllTimeBestContest/AllTimeBestContest";
import ClientFeedBack from "../ClientFeedBack/ClientFeedBack";
import Footer from "../Footer/Footer";

const Home = () => {
  return (
    <div className="font-poppins  md:p-3">
      <Helmet>
        <title>Contest Master | Home</title>
      </Helmet>

      <Navbar />
      <Banner />
      <ShowContestWinner/>
      <ContestType/>
      <MiddleBanner/>
      <AllTimeBestContest/>
      <ClientFeedBack/>
      <Footer/>
      

    </div>
  );
};

export default Home;
