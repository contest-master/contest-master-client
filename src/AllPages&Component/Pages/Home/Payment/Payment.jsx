import { useParams } from "react-router-dom";
import useSingleContest from "../../../Hooks/useSingleContest";
import useAuth from "../../../Hooks/useAuth";
import CheckOut from "../../../Components/CheckOut/CheckOut";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";


const stripPromise = loadStripe('pk_test_51PKtby    2KCmLBsrFWm23P5SPHmQ0QQ7clg5aELZse7ZB9JTcXr4G8cAEtDxpHUUyt8wjqF89G5nRPjPcDi9Ox7y4q00EemO5jSi')
const Payment = () => {
    const {id} = useParams()
    const {user} = useAuth()
    const {data:contestData, isLoading} = useSingleContest(id)

  

    console.log(contestData,user);




    return (
        <div>
            <Elements stripe={stripPromise}>
                
                <CheckOut/>
            </Elements>
            
        </div>
    );
};

export default Payment;