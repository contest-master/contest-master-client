import React from 'react';
import useAllContest from '../../../Hooks/useAllContest';
import TitleAndSubtitle from '../../../Components/utils/TitleAndSubtitle';
import './alltimecss.css'
import { Link } from 'react-router-dom';
const AllTimeBestContest = () => {
    const {allContest,isLoading} = useAllContest()
    const allQty = allContest?.sort((a,b)=>b.qty-a.qty)?.slice(0,4)
    const overlayStyle = {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.5)', // Adjust the opacity as needed
        // Adjust the opacity as needed
      };

    return (
        <div className='font-poppins'>
            <TitleAndSubtitle title={'All Time Best Contests'} subTitle={'All values are real time updating'}/>
          
          <div className='grid md:grid-cols-2 gap-5 rounded-xl md:p-5 bg-slate-100'>
          {
                allQty?.map((d,idx)=>(
                    <div key={idx}>
                                <div className='w-full mx-auto  '>
                                 <div style={{backgroundImage:`url(${d.image})`}}  className='relative h-80 cover  mx-auto w-full rounded-xl' >
                                    <div style={overlayStyle} className='z-50 absolute rounded-xl'>

                                        <h1 className='text-3xl mt-5 text-center text-white'> {d.contestType} </h1>
                                        <div className='p-2 mt-20 flex flex-row items-center'>
                                        <div>
                                        <h1 className='text-white font-semibold text-xl'>Total Participates</h1>
                                          <div className='box relative flex items-center justify-center'>
                                            <h1 className='text-purple-600 text-5xl   z-50'>{d.qty}</h1><sub className='text-purple-600'>Times</sub>
                                            
                                        </div>
                                         
                                          </div>
                                          <Link to={`/contestDetails/${d?._id}`} >
                                          <button className='gradient-button w-auto h-11 ml-10'>Click to participate</button></Link>
                                        

                                        </div>

                                    </div>

                                 </div>

                                    
                                  
                            </div>
                        </div>
                    
                ))
            }
          </div>
        </div>
    );
};

export default AllTimeBestContest;