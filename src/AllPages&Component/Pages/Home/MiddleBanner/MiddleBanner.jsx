import React from 'react';
import { FaUserTie } from "react-icons/fa";
import { FaUsers } from "react-icons/fa";
import { MdQuiz } from "react-icons/md";
import useCounter from '../../../Hooks/useCounter';
import LoadingComponent from '../../../Components/LoadingComponent/LoadingComponent';


const MiddleBanner = () => {
    const {count,counterError,counterLoading} = useCounter()
    if(counterLoading){
    return <LoadingComponent/>
    }

  









    const divStyle = {
        position:'relative',
        backgroundImage: 'url("bg.webp")',
        backgroundSize: 'cover', 
        backgroundRepeat: 'no-repeat', 
        height: '400px', 
        width: '100%',
      };

      const overlayStyle = {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.7)', // Adjust the opacity as needed
      };

    return (
        <div style={divStyle} className='font-poppins my-20 rounded-md shadow-md'>
            <div style={overlayStyle} className='flex justify-center rounded-md items-center'>
                    <div className='' >
                    <div className='text-white'>
                       <h1 className='text-center text-3xl md:text-5xl font-semibold'>Why Making Late? Join Fast and <br className='hidden md:block' /> Claim Your Rewards</h1>
                       </div>


                       <div className='flex justify-around mt-12 items-center '>
                        
                       <div className='text-white text-center'>
                        <FaUserTie className='text-6xl md:text-[100px] mx-auto'/> 
                        <p className='text-lg md:text-2xl'>Total Participates</p>
                        <p className='text-xl md:text-2xl'>{count?.qty}</p>
                       </div>

                       <div className='text-white text-center'>
                        <FaUsers className='text-6xl md:text-[100px] mx-auto'/> 
                        <p className=' text-lg md:text-2xl'>Total Contest</p>
                        <p className=' text-lg md:text-2xl'>{count?.contest}</p>
                       </div>

                       <div className='text-white text-center'>
                        <MdQuiz className='text-6xl md:text-[100px] mx-auto'/> 
                        <p className='text-xl md:text-2xl'>Total User</p>
                        <p className='text-xl md:text-2xl'>{count?.participates}</p>
                       </div>


                       </div>






                    </div>
            </div>
        </div>
    );
};

export default MiddleBanner;