import { useState } from "react";
import { GiHamburgerMenu } from "react-icons/gi";
import { RxCross2 } from "react-icons/rx";
import ThemeToggle from "../../../Utils/ThemeToggle";
import { Link, NavLink } from "react-router-dom";
import useAuth from "../../../Hooks/useAuth";
import toast from "react-hot-toast";
import { FaCartShopping } from "react-icons/fa6";
import useUserParticipatedData from "../../../Hooks/useUserParticipatedData";

const Navbar = () => {
    const [show,setShow] = useState(false)
    const {user,logOut} = useAuth()
   
 
    const {part,refetch} = useUserParticipatedData()
  

    const handleLogout=()=>{
        logOut()
        .then(() => {
            return toast.success('Logout done')   
        }).catch((err) => {
            console.log(err);
        });
    }



refetch()
    const nav=<>
    {!user &&<li><NavLink to='login'>Login</NavLink></li>}
    <li><NavLink></NavLink></li>
    </>

    const dashNav=<>
    <ul className="list-none">
    <li><NavLink to='dashboard' className={`hover:text-white active:text-red-500`}>Dashboard</NavLink></li>
    </ul>
    </>

    return (
      <div className="relative">
          <div className=" text-right p-2 md:p-1 flex justify-between items-center  ">

        {/* logo */}
            <div className=" ">
              <Link to='/'>  
              <h1 className="text-[#7D0DC3]  text-2xl  font-semibold">Contest Master</h1>
              </Link>
            </div>

            {/* toggle navbar big screen*/}
          <div className="flex items-center gap-2 ">
            {
                user?<>
                <div className="">
               <Link className="flex text-xl font-semibold" to={'dashboard/cart'}><sup  className="mt-2 purpleText">{part?.length}</sup> <FaCartShopping className="text-xl text-red-700"/></Link>
                </div>
                <img className="w-10 h-10 ring ring-[purple]  rounded-full" src={user.photoURL } alt="" />
                </>
                :' '

            }
          <button className="mr-5 text-xl purpleText" onClick={()=>setShow(!show)}>
                        {
                            show?<RxCross2/>:<GiHamburgerMenu/>
                        }
            </button>
            {/* hidden navbar big screen */}
          </div>
            <div className={` absolute z-50 right-0 top-[50px] rounded-lg    transform transition-all ease-in-out duration-300 ${show?'block':'hidden'} purple-bg  border border-white  w-40 text-center h-48 text-white shadowOne   `}>
                <ul className="my-2  text-white">
                    {nav}
                </ul>

                {user &&
                <>
                {dashNav}
                <div className=" ">
                <li> <ThemeToggle/> </li>

                <h1>{user?.displayName}</h1>
                <button className="border px-4 py-1 mt-3 border-white rounded-xl" onClick={handleLogout} >LogOut</button>
                </div>

                </>}

            </div>
          </div>

                        {/* mini screen */}
        {/* <div className=" ">
                <div className= {` absolute text-center top-0 md:hidden purple-bg  z-50 w-2/5 ${show?'translate-x-0':'-translate-x-full'} transition-transform delay-200 h-screen`}>
                   <div className="flex flex-col justify-between  h-full">
                   <div className="mt-5">
                   <ul className=" text-white">
                 
                {user ?
                <>
                {dashNav}
                </>:''}

                        <li> <ThemeToggle/> </li>
                    </ul>
                   </div>
                   <div className="mb-4">
                   <h1 className="text-white ">{user?.displayName}</h1>

                   <button className="border text-white px-2 border-white rounded-xl" onClick={handleLogout} >LogOut</button>
                   </div>
                   </div>
                </div>
               
            </div> */}



      </div>
       
    );
};

export default Navbar;