
import {  Link, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form"
import { useState } from "react";
import useAuth from "../../Hooks/useAuth";
import toast from "react-hot-toast";
import useUploadImg from "../../Hooks/useUploadImg";
import useAxios from "../../Hooks/useAxios";


const SignUp = () => {
    const {  imageUrl, uploadImage,imageUploadLoading}= useUploadImg()
    const [imagePreview,setImagePreview] = useState(null)
    const [userImg,setUserImg] = useState(null)
    const {createUser ,loading ,updateUser} = useAuth()
    const navigate = useNavigate()
    const   axiosOpen = useAxios()
    const {
      register,
      handleSubmit,
      watch,
      formState: { errors },
      } = useForm()

      const waitForImageUrl = () => {
        return new Promise((resolve) => {
            const interval = setInterval(() => {
                if (imageUrl) {
                    clearInterval(interval);
                    resolve(imageUrl);
                }
            }, 100);
        });
    };

        const onSubmit =async (data) =>{
            const {email,name , password} = data
            const role="user"

            const { image } = data;
            const imageFile = image[0];

            
          // image upload
              try {
                await uploadImage(imageFile);
               await waitForImageUrl()
                setUserImg(imageUrl)
               } 
              catch (uploadError) {
                console.error('Error uploading image:', uploadError);
               }

               if(imageUrl){
                await createUser(email,password)
                .then(res=>{
                   updateUser(name,imageUrl)
                   .then(async()=>{
                      const role = 'User'
                      const userProfile ={
                          email,name,imageUrl,role
                      }
      
                      await axiosOpen.post('/user-data',userProfile)
                        .then(res=>{
                          toast.success('User Create Done')
                          return navigate('/')
                        })
                  
                   })
                   .catch(err=>console.log(err))
                   
               
                })
                .catch(err=>console.log(err))
               }
            
            

         
               
            

      console.log(errors);



            const userProfile = {
                name,
                email,
                userImg,
                role,
            }




          
    }

 







    return (
        <div>
            {/* <UseHelmet title={'SignUp'}/> */}
          <div>
            {
              imageUploadLoading && <> <h1 className="text-blue text-2xl tex-center">.......Loading</h1> </>
            }
          </div>
            <div className="hero min-h-screen bg-base-200">
  <div className="hero-content  ">

    <div className="card shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
       
      <form className="card-body" onSubmit={handleSubmit(onSubmit)}>
      <div className="w-full h-full mx-auto ">
       {
            imagePreview && <>
            <div>
                <h1 className="text-center font-poppins text-lg font-semibold ">Welcome to you</h1>
                <img    className="h-20 w-20 rounded-full mx-auto" src={imagePreview} alt="" />
            </div>
            </>
        }
       </div>
      <div className="form-control">
          <label className="label">
            <span className="label-text">Name</span>
          </label>
          <input type="text" name="name" {...register("name",{required:'Name must be required'})} placeholder="full name" className="input input-bordered" />
          <p className='text-red-500'>{errors.name?"Name is required" : ''}</p>
    
        </div>

        <div className="form-control ">
          <label className="label">
            <span className="label-text">Image</span>
          </label>
          <div className="">

          <input 
           type="file" name="image" {...register("image", {required:true} )} placeholder="image" className="mt-2"
           onChange={ (e)=> setImagePreview(URL.createObjectURL(e.target.files[0]))} />
                     <p className='text-red-500'>{errors.image?"Photo URL is required" : ''}</p>
          </div>
        
        </div>


        <div className="form-control">
          <label className="label">
            <span className="label-text">Email</span>
          </label>
          <input type="email" placeholder="email" name="email" {...register('email')} className="input input-bordered"  />
          <p className='text-red-500'>{errors.email && "Email is required" }</p>

        </div>

       <div className="form-control">
          <label className="label">
            <span className="label-text">Password</span>
          </label>
          <input type="password" {...register("password", {
            required:true,
            maxLength:12,
            minLength:6,
            pattern:/^(?=.*[A-Z])(?=.*[a-z])(?=.*[\W_]).+$/
          } )} placeholder="password" className="input input-bordered" required />
        </div>

        <p className='text-red-500'>{errors.password?.type==="required" && "password is required" }</p>
        <p className='text-red-500'>{errors.password?.type==="maxLength" && "password is must be less then 12" }</p>
        <p className='text-red-500'>{errors.password?.type==="minLength" && "Minimum 6 letter required" }</p>
        <p className='text-red-500'>{errors.password?.type==="pattern" && "Password need minimum one uppercase , one lower case and one special character" }</p>



        <div className="form-control mt-6">
          <button className="btn btn-primary">SignUp</button>
        </div>
      </form>
      <div className="p-2 -mt-5 text-center">
        <Link to='/login'><p>Already have an account Please <span className="font-semibold text-blue-500">Login</span></p></Link>
      </div>
    </div>
  </div>
</div>
        </div>
    );
};

export default SignUp