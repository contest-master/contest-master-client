import { set } from "date-fns";
import useUserParticipatedData from "../../../Hooks/useUserParticipatedData";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import { useState } from "react";
import JoinContestShow from "./JoinContestShow";
import useContestResult from "../../../Hooks/useContestResult";



const JoinContest = () => {
    const { join ,refetch} = useUserParticipatedData();
    const {contestResult,contestResultFetch} = useContestResult()
    
    
    const id = Array.from(new Set(contestResult?.map(d=>d?.contestSubmitId)))
    
    const newLength = join?.filter(d=>!id.includes(d._id))
    const uniqueContest = Array.from(new Set(newLength?.map(d=>d?.contestType)))


  contestResultFetch()
  refetch()
  return (
    <div className="pt-2 w-full">
    {
        newLength?.length<=0?<>
                 <div className="w-full h-screen flex-col text-center flex items-center justify-center">
                        <p className="text-2xl"> 😴 Opppos </p> <br />
                        <h1 className="text-4xl font-poppins">Contest no found </h1>
                    </div>
        </>:<>
        <div>
    <div className="text-center mt-5 font-poppins">
        <h1 className=" text-2xl md:text-3xl purpleText font-semibold">
          You have remaining {newLength?.length} contests to participate
        </h1>
        <p className="text-center font-semibold">
          Please submit all tasks before date is end
        </p>
      </div>

      <div>
            <Tabs>
            <TabList className={'font-semibold  purpleText mt-10 text-sm mx-auto w-full text-center'}>
                {
                    uniqueContest?.map((d,idx)=>(<Tab 
                        key={idx} ><button>{d}</button></Tab>))
                }
            </TabList>

                {
                    uniqueContest?.map((type,idx)=>(
                        <TabPanel key={idx}>

                            {
                                join?.filter(d=>d?.contestType ===type && !id.includes(d._id) )?.map((d,idx)=>(
                                    <div className="" key={idx}>
                                        <JoinContestShow data={d} />
                                    </div>
                                ))
                            }
                        </TabPanel>
                    ))
                }
            
            </Tabs>
      </div>
    </div>
        </>
    }
    </div>
  );
};

export default JoinContest;
