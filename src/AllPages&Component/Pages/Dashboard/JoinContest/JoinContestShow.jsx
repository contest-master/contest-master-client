import { useMutation } from '@tanstack/react-query';
import React, { useState } from 'react';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
import useAxiosSecure from '../../../Hooks/useAxiosSecure';
import toast from 'react-hot-toast';
import useContestResult from '../../../Hooks/useContestResult';

    const JoinContestShow = ({data}) => {
        const [textValue,setTextValue] = useState('')
        const axiosSecure = useAxiosSecure()
        const {contestResultFetch} = useContestResult()

        const ContestWinningPrice = data.ContestWinningPrice
        const contestId = data.contestId
        const contestName = data.contestName
        const image = data.image
        const participateEmail = data.participateEmail
        const participateImage = data.participateImage
        const participateName = data.participateName
        const paymentApproval = data.paymentApproval
        const publishDate = data.publishDate
        const endDate = data.endDate
        const contestSubmitId = data._id 
        const result ="pending"
        const {publisher} =  data
        const currentTime = new Date()
        const submittedTime = currentTime.toLocaleString()
        const submittedDate = currentTime.toLocaleDateString()
        


console.log();

        const {mutateAsync:results} = useMutation({
              mutationKey:['contestTaskSubmitted'],
               mutationFn:async({submittedTask})=>{
                const {data}=await axiosSecure.post('/contestTaskSubmit',{submittedTask})
                return data
            }
        })

        const handleContestSubmit=async(e)=>{
            e.preventDefault()
            const from = e.target
            const submit = from.submit.value;

            const submittedTask = {
                ContestWinningPrice,contestId,contestName,participateEmail,image,participateImage,participateName,paymentApproval,
                publishDate,endDate,contestSubmitId,submit,publisher,result,submittedDate,submittedTime
            }
            console.log(submittedTask,'submitedd teas');
            
            try {
                const result = await results({submittedTask})
                if(result.insertedId){
                    contestResultFetch()
                    return toast.success("Your contest submit done")
                }
            } catch (error) {
                console.log(error);
            }
        }

    return (
        <div className='mb-10 '>
            <Accordion>
            <AccordionItem >
            <AccordionItemHeading>
                <AccordionItemButton >
                    {data?.contestName}
                </AccordionItemButton>
            </AccordionItemHeading>

            <AccordionItemPanel className='w-full'>
              <div className='border border-purple-700 p-2 rounded-lg w-full font-poppins' >
                <div className='md:flex  w-full'>
                    <div className='w-full md:w-1/2'>
                    <img className='w-56' src={data?.image} alt="" />
                        <div className='mt-2'>
                        <h1 className='font-semibold'>Contest Type : <span className='font-medium'>{data?.contestType}</span> </h1>
                        <h1 className='font-semibold'>Publish Date : <span className='font-medium'>{data?.publishDate}</span> </h1>
                        <h1 className='font-semibold'>End Date: <span className='font-medium'>{data?.endDate}</span> </h1>
                        <h1 className='font-semibold'>Contest Winning Price: <span className='font-medium'>{data?.ContestWinningPrice}</span> </h1>

                        </div>
                    </div>
                    {/* ---------------- */}
                    <div className=' border border-purple-700 bg-slate-300 shadowOne rounded-xl flex-1 py-5'>
                        <h1 className='text-center text-2xl border-b-2 border-purple-700 purpleText '>Submit Your Task</h1>

                        <div className='px-2 mt-4'>
                            <p className='text-sm'><span className='font-semibold '>Your Task</span> : {data?.taskSubmissionInstructions}</p>
                            <div className='mt-2'>
                                <input type="file" />

                                <p className='text-center my-3'>-------- OR -----------</p>
                                <p className='text-sm'>**Submit written within 250 words About related topic: </p>
                              <form onSubmit={handleContestSubmit}>
                              <textarea name='submit' value={textValue} onChange={(e)=>setTextValue(e.target.value)} cols={37} rows={5} className='p-2  purpleText'> </textarea> <br /> 
                                {
                                    textValue?.length>0 && <button type='submit'  className=' btn  gradient-button  disabled '>Submit Your Task</button>
                                }
                              </form>

                            </div>
                        </div>

                    </div>
                </div>
              </div>
            


            </AccordionItemPanel>
        </AccordionItem>
            </Accordion>
         


           
        </div>
    );
};

export default JoinContestShow;