import Dropdown from "../../../Components/Dropdown/Dropdown";
import useDbUserData from "../../../Hooks/useDbUserData";
import './role.css'
const AllUsers = () => {

    const {dbUser,dbUserLoading,dbUserError} = useDbUserData()








   
console.log(dbUser);

    return (
        <div className="font-poppins">
            
        <div>
            <h1 className="text-3xl font-semibold text center my-6 text-center underline">User Management</h1>
        </div>
        {/* Table */}
        <div className="overflow-x-auto overflow-y-auto">
  <table className="table">
    {/* head */}
    <thead>
      <tr className="text-center text-base border border-red-500 ">
        <th>*</th>
        <th>Name</th>
        <th>Email</th>
        <th>Role</th>
        <th>Mange User</th>
      </tr>
    </thead>
    <tbody>
      {/* row 1 */}
   {
    dbUser?.map((d,idx)=>(
        <tr key={idx} className="bg-base-200 text-center">
        <th>{idx+1}</th>
        <td>{d.name}</td>
        <td>{d.email}</td>
        <td className="parent">
           <span
           className={`
            ${d.role==="Moderator" && 'role-moderator'}
            ${d.role==="User" && 'role-user'}
            ${d.role==="Admin" && 'role-admin'}

           `}> {d.role}</span>
        </td>
        <td>
        <Dropdown userId={d._id} />
        </td>
      </tr>
    ))
   }
    </tbody>
  </table>
</div>


        </div>
    );
};

export default AllUsers;