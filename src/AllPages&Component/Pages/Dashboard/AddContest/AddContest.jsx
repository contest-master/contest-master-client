    import { ContestForm } from "../../../Components/ContestForm/ContestForm";
    import { useEffect, useState } from "react";
    import useUploadImg from "../../../Hooks/useUploadImg";
    import useAxiosSecure from "../../../Hooks/useAxiosSecure";
    import  { useQuery }  from "@tanstack/react-query"
    import useAuth from "../../../Hooks/useAuth";
    import { useNavigate } from "react-router-dom";
    import toast from "react-hot-toast";

const AddContest = () => {
    const navigate = useNavigate()
    const [dates,setDates] = useState( {
        startDate: new Date(),
        endDate: new Date(),
        key: 'selection'
      })
    const {imageUrl,uploadImage,  error,imageUploadLoading} = useUploadImg()

    const [imgFile,setImageFile] = useState(null)



    const {user} = useAuth()
    const email = user?.email
    

    const axiosSecure = useAxiosSecure()
    const {data:userData} = useQuery({
        queryKey:['users',email],
        queryFn:async()=>{
            const {data} = await axiosSecure.get(`/user-data/${email}`)
            return data
         }
    })
 

    const handleDate = item=>{
        setDates(item.selection)
    }

    
    const handleUp=async()=>{
        try {
            await uploadImage(imgFile)
            return imageUrl
        } catch (error) {
            console.log(error);
        }

    }  
    useEffect(()=>{
        if (imgFile) {
            handleUp();
        }
    },[imgFile])
    
  
    const handleSubmit =async (e) =>{
        e.preventDefault()
        const form = e.target
        const contestDescription = form.contestDescription.value
        const contestName= form.contestName.value
        const priceValue = form.contestPrice.value
        const price = parseFloat(priceValue)
        const prize = form.prize.value
        const contestType = form.contestType.value
        const taskSubmissionInstructions = form.taskSubmissionInstructions.value

        const publishDate = dates.startDate.toLocaleDateString()
        const endDate = dates.endDate.toLocaleDateString()


        
       
        const contest = {
            publisher:{
                name:userData.name,
                email:userData.email,
                publisherImage:userData.imageUrl,
                role:userData.role
            },
            contestName,
            contestDescription,
            prize,
            price,
            contestType,
            taskSubmissionInstructions,
            publishDate,
            endDate,
            image:imageUrl,

        }
        console.log(contest);
        
        const {data} = await axiosSecure.post('/contest',contest)
            if(data.insertedId){
             
                return toast.success('Contest Post Done')
            }
        
    }


    return (
        <div>

            {/* from */}
            
            <ContestForm
             handleSubmit={handleSubmit}
             dates={dates}
             handleDate={handleDate}
             setImageFile={setImageFile}
             />
    
        </div>
    );
};

export default AddContest;