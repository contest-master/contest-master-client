import { Link,  } from "react-router-dom";
import useUserParticipatedData from "../../../Hooks/useUserParticipatedData";

const ParticipatedCart = () => {
    const {part} = useUserParticipatedData()

    return (
        
        <div className=" font-poppins">
            <div>
                {
                    part?.length<=0?<>
                    <div className="md:w-4/5    w-full h-screen flex-col text-center flex items-center justify-center">
                        <p className="text-2xl"> 😴 Opppos </p> <br />
                        <h1 className="text-4xl font-poppins">Contest not found in Your cart</h1>
                    </div>
                    </>
                    :
                    <>
                    <div className="pt-3">
                    <h1 className="text-center text-3xl purpleText ">Please Pay to Confirm to Your Participation</h1>
            {/* right- cart product */}
            <div className="md:grid   grid-cols-3 mx-auto  w-full md:w-4/5">
        {
                part?.map((d,idx)=>(
                    <div key={idx} className="card my-4 mr-4 border-purple-900 border bg-base-100 shadow-xl ">
                    <figure><img src={d.image} className="h-44 w-full" alt="Shoes" /></figure>
                    <div className="p-2 font-poppins ">
                        <div className=" text-[12px] font-semibold">
                            <p>Start Date : {d.publishDate}</p> 
                            <p  className="mt-1">End Date : {d.publishDate}</p>
                        </div>

                        <div className="flex flex-col ">
                            <div className="h-36">
                                
                      <p className="text-md text-[13px] font-semibold "> Category : <span className="font-medium font-poppins">{d.contestType}</span> </p>
                      <h1 className="text-[13px] font-semibold mt-1">Contest Name: <span className="font-medium">{d.contestName}</span></h1>
                      <p className="text-md text-[14px]  font-semibold"> Price : {d.contestPrice}$</p>
                      <p className="text-sm font-semibold">Payment Status: <span className="font-medium">
                      {d.paymentApproval==="pending"?'Please wait for approve' 
                      : d.paymentApproval==="Approve"?'Approved':'Payment pending'}
                        </span></p>
                            </div>
                            <div className="">
                            {
                        d.paymentApproval?'': <Link to={`/payment/${d.contestId}`} className="text-center mx-auto  w-full"><button className=" mt-2 gradient-button w-full ">Payment to participate</button></Link>
                     }
                   
                            </div>
                        </div>

                     
                    </div>
                    </div>
                ))
            }

           </div>

                    </div>
                    </>
                }
            </div>
     
        </div>
    );
};

export default ParticipatedCart;