import React, { useEffect } from 'react';
import { useState } from "react";
import useUploadImg from "../../../Hooks/useUploadImg";
import useAxiosSecure from "../../../Hooks/useAxiosSecure";
import  { useMutation,  }  from "@tanstack/react-query"
import useAuth from "../../../Hooks/useAuth";
import { useNavigate, useParams } from "react-router-dom";
import toast from "react-hot-toast";
import { DateRange } from 'react-date-range';
import { category } from '../../Home/ContestType/category';
import useMyContest from '../../../Hooks/useMyContest';
import TitleAndSubtitle from '../../../Components/utils/TitleAndSubtitle';
import LoadingComponent from '../../../Components/LoadingComponent/LoadingComponent';

const MyContestUpdate = () => {
    const {id} = useParams()
    const {user} = useAuth()
    const email = user?.email
    const [data,setData] = useState()
    const [dates,setDates] = useState( {
        startDate: new Date(),
        endDate: new Date(),
        key: 'selection'
      })
    const {myContest,isLoading} = useMyContest({email})
    const contestData = myContest?.find(d=>d._id===id)
    const axiosSecure = useAxiosSecure()
    const [images,setImages] = useState(null)
    const {uploadImage,imageUrl,imageUploadLoading} = useUploadImg()
    const [loading,setLoading] = useState(null)




    const {mutateAsync:updateContest} = useMutation({
        mutationFn:async(contest)=>{
            const {data} = await axiosSecure.put(`/contest/${id}`,contest)
            return data
        },

        onSuccess:()=>{
            setLoading(false)
            toast.success('Contest Updated')
             navigate(-1)
            
            
            //too--do swal
        },

        onError:(error)=>{
            setLoading(false)
            return toast.error(error.message)
        }
    })
  

    useEffect(()=>{
        if(contestData){
            setData(contestData)
            setImages(contestData.image)
            if (contestData.publishDate && contestData.endDate) {
                setDates({
                    startDate: new Date(contestData.publishDate),
                    endDate: new Date(contestData.endDate),
                    key: 'selection'
                });
            }   
        }
        console.log();
    },[contestData])

    const navigate = useNavigate()


      

    const handleOnChange = e =>{
        const imageFile = e?.target?.files[0]
        if(imageFile){
            setImages(imageFile)}
            else{
                setImages(data?.image)
            }
    }

 
    const handleSubmit=async(e)=>{
        e.preventDefault()
        setLoading(true)
        if(images){
            uploadImage(images)
        }
        const form = e.target
        if(imageUploadLoading){
            setLoading(false)
            return toast.error('Image is still uploading, please wait...');

        }

        const contestName = form.contestName.value
        const contestDescription = form.contestDescription.value
        const priceValue = form.price.value
        const price = parseFloat(priceValue)
        const prize = form.prize.value
        const contestType = form.contestType.value
        const taskSubmissionInstructions = form.taskSubmissionInstructions.value

        const publishDate = dates.startDate.toLocaleDateString()
        const endDate = dates.endDate.toLocaleDateString()

            const contest = {
                contestName,
                contestDescription,
                price,
                prize,
                contestType,
                taskSubmissionInstructions,
                publishDate,
                endDate,
                image:imageUrl,
            }
            if (!imageUrl) {
                setLoading(false);
                return toast.error('Image not uploaded, please try again');
            }
    
            try {
                 await updateContest(contest)
                
            } catch (error) {
                setLoading(false)
            }
    }
    if (imageUploadLoading) {
        return<LoadingComponent/>
        
    }
    return (
        <div className='-mt-20'>
            <TitleAndSubtitle title={'Update Your Contest'}/>
             <div className='border-2 md:w-4/5 mx-auto p-2 border-purple-600 rounded-lg text-center '>
                <form onSubmit={handleSubmit}>

                <div className='flex flex-col md:flex-row  '>
                        
                        <div className='w-full'>
                        <input type="text" name='contestName' defaultValue={data?.contestName} placeholder="Contest Name" className="input input-bordered input-error w-full max-w-xs" />
                        <DateRange
                    
                    rangeColors={["#F43F5e"]}
                    editableDateInputs={true}
                    onChange={item => setDates(item.selection)}
                    moveRangeOnFirstSelection={false}
                    ranges={[dates]}  
                />

                        </div>


                        <div className='w-full'>
                        <input type="number" name='price' defaultValue={data?.price} placeholder="Contest Price" className="input input-bordered input-error w-full max-w-xs my-3" />

                        <select name='contestType'  className="select my-3 select-primary input input-bordered input-error w-full max-w-xs ">
                            {
                            category?.map((d,idx)=>(<option defaultValue={data?.contestType} key={idx} value={data?.contestType} > {d.contestType} </option>))
                            } 
                        </select>

                        <input type="text" defaultValue={data?.prize} name='prize' placeholder="Wining Price" className="input my-3 input-bordered input-error w-full max-w-xs" />
                            <input type="text" name='contestDescription' defaultValue={data?.contestDescription} placeholder="Description" className="input input-bordered input-error my-3 w-full  max-w-xs" />
                            <input type="text" name='taskSubmissionInstructions' placeholder="Task Instruction" defaultValue={data?.taskSubmissionInstructions} className="input my-3 input-bordered input-error w-full max-w-xs" />
                            <div className='text-left ml-16 my-3'>

                            <input onChange={handleOnChange} type='file' name='image'/>
                </div>

                <button className='btn btn-primary'>submit</button>

                        

                        </div>
                        

                </div>


                </form>
            </div>
        </div>
    );
};

export default MyContestUpdate;