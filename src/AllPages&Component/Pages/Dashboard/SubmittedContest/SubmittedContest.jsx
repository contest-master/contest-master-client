import toast from "react-hot-toast";
import useContest from "../../../Hooks/useContest";
import useContestResult from "../../../Hooks/useContestResult";
import { useRef, useState } from "react";
import { useMutation } from "@tanstack/react-query";
import useAxiosSecure from "../../../Hooks/useAxiosSecure";

const SubmittedContest = () => {
  const [inputValue,setInputValue] = useState(null)
  const { selectContestType } = useContest();
  const axiosSecure = useAxiosSecure()

  const { myContestParticipated, contestResultFetch,contestResultLoading } = useContestResult();
  const myContestResultSubmitPending = myContestParticipated?.filter(d=>d.result==="pending")
  

  const {mutateAsync} = useMutation({
    mutationKey:"result",
     mutationFn:async({submitResult})=>{

         await axiosSecure.patch(`/result`,submitResult)
         .then((result) => {
          if(result.data.modifiedCount>0){  
            contestResultFetch()
            setInputValue(0)
            toast.success("Result post done")
          }
          console.log(result);
         }).catch((err) => {
          console.log(err);
         })
     }
  })

  //my uploaded contest and also pending contest
  const contestAccordingName = selectContestType
    ? myContestParticipated?.filter((d) => d.contestName === selectContestType && d.result==="pending") : myContestResultSubmitPending;

    const handleResult = async(d)=>{

      const id = d._id

      const value = parseInt(inputValue)
      const result = value
      if(result<=0){
        return toast.error('Please select valid Result')
      }

      const contestName = d.contestName
      
      const contest = myContestParticipated?.filter(d=>d.contestName===contestName && d.result!=="pending")
      const results = contest?.map(d=>d.result)
      const valid = results.find(d=>d===result)
      if(valid){
        return toast.error('Not a valid result,You can not make same result for different participate')
      }
      const submitResult={
        id, contestName ,result
      }

    await mutateAsync({submitResult})
  
     
     }
     
     
     


  return (
    <div className="pt-5">
        {
          contestAccordingName.length<=0?<>
          <div className="w-full h-screen flex items-center justify-center">
            <h1 className="text-purple-900 font-semibold text-2xl">Not found any contest</h1>
          </div>
          </>
          :
          <>
<h1 className="text-center text-4xl my-2 mb-3 font-poppins purpleText">It s time to choose perfect winners</h1>
      <div className="md:grid grid-cols-3 gap-5 mx-4 md:mx-0 font-poppins">
      {contestAccordingName?.map((d, idx) => (

        <div key={idx} className="mt-5">
          <div className="card w-full h-80 bg-base-100 border border-purple-500 shadow-xl overflow-y-auto">
            <div className="card-body">
               <div className="flex items-center gap-2 ">
               <div className="">
               <img className="w-16 rounded-full border border-purple-400" src={d.participateImage} alt="" />
               </div>
               <div>
                <p className="text-sm font-semibold text-slate-600">{d.participateName}</p>
                <p className="text-sm font-semibold text-slate-600">{d.participateEmail}</p>
               </div>
               </div>
                <p className="text-sm"> Submitted at : {d.submittedTime}</p>
              <h2 className="card-title">Contest Name: {d.contestName}</h2>
              <h2 className="card-title">Participate Result: </h2>
              <p><span className="font-bold">Submit:</span>{d.submit}</p>
              <div className="card-actions justify-center   ">
                <input  onChange={(e)=>setInputValue(e.target.value)} type="number" name="inputType" className="border-purple-600 border rounded-lg text-center text-purple-900 font-semibold" />
                <button onClick={()=>handleResult(d)}  type="submit"  className="p-4  gradient-button "><span  className="text-sm py-2 px-3">Place Rank</span></button>
              </div>
            </div>
          </div>{" "}
        </div>
      ))}
      </div>
          </>
        }
    </div>
  );
};

export default SubmittedContest;
