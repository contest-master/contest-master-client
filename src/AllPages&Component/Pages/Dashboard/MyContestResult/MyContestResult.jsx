import useAuth from "../../../Hooks/useAuth";
import useContestResult from "../../../Hooks/useContestResult";

const MyContestResult = () => {
    const { contestResult} = useContestResult()
    const {user} = useAuth()
    const email = user?.email
    const myData= contestResult?.filter(d=>d.participateEmail===email)
    console.log(myData);
    return (
        <div className="w-full" >
            {
                myData?.length<0?<>
                              <div className="w-full h-screen flex-col text-center flex items-center justify-center">
                        <p className="text-2xl"> 😴 Opppos </p> <br />
                        <h1 className="text-4xl font-poppins text-purple-700">No Contest Found</h1>
                    </div>
                </>
                :
            
    <div>
         <div className="text-center text-2xl font-poppins ">
  <h1>Text Keep your Eye on Result</h1>
  <h1>It will publish within short time</h1>
  </div>
  <div className="md:grid grid-cols-3 gap-5 pt-2 ">
 
 {
    myData?.map((d,idx)=>(
         <div key={idx} className="mt-10  mx-4 md:mx-0  ">
             <div className="purple-bg rounded-2xl h-[380px]  hover:bg-purple-700 text-white font-poppins p-3 ">
               <div className="border purple-bg border-white h-[360px] p-3 rounded-lg">
               <div className="mb-5">
                 <img width={100} className="rounded-full mx-auto" src={d.image} alt="" />
                 </div>
                 <p  className="font-semibold"><span  className="font-medium">Participate Name:</span>{d.participateName}</p>
                 <p  className="font-semibold"><span  className="font-medium">Participate Email:</span>{d.participateEmail}</p>
                 <p  className="font-semibold"><span  className="font-medium">Contest Name:</span>{d.contestName}</p>
                 <p  className="font-semibold"><span  className="font-medium">Publish Date:</span>{d.publishDate}</p>
                 <p className="font-semibold"><span className="font-medium">End Date:</span>{d.endDate}</p>
                 <p className="font-semibold"><span className="font-medium">My Submit:</span>{d.submit.slice(0,20)}</p>
               
                 <p className="font-semibold"><span className="font-medium">My Result:
                     </span>{d.result==="pending"?"Pending":d.result}</p>
               </div>
             </div>
         </div>
     ))
 }
</div>  
    </div>            

             
            }

        </div>
       
    );
};

export default MyContestResult;