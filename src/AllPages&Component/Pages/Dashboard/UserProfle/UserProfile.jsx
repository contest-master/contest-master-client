import useAuth from "../../../Hooks/useAuth";
import useDbUserData from "../../../Hooks/useDbUserData";

const UserProfile = () => {
  const { currentUser, dbUserLoading, dbUserError } = useDbUserData();


  return (
    <div className="w-full pt-1">
      <h1 className="text-center font-semibold mt-16 font-poppins text-2xl">
        Hello ,<span className="text-purple-700  ">{currentUser?.name}</span>{" "}
        Welcome to your profile
        <div className=" mt-5 mx-auto  mr-5 md:mr-0 flex justify-center  md:h-96">
          <div className="max-w-sm p-6  text-white inset-11 transition-transform border-2 shadowOne border-purple-800 rounded-lg shadow purpleBg dark:border-gray-700">
            <h1 className="  text-2xl  font-semibold">
              Contest Master
            </h1>
            <hr className="purple-bg h-[2px] text-white" />
            <p className="mt-4 text-xl">User Profile</p>
            <img
              className="w-28 mt-2 mx-auto rounded-full"
              src={currentUser?.imageUrl}
              alt=""
            />
            <div className="text-left mt-4">
              <p className="text-lg">Name : {currentUser?.name}</p>
              <p className="text-sm">Email : {currentUser?.email}</p>
              <p className="text-sm">Role : {currentUser?.role}</p>
              <p className="text-sm">Id: {currentUser?._id}</p>
            </div>
          </div>
        </div>
      </h1>
    </div>
  );
};

export default UserProfile;
