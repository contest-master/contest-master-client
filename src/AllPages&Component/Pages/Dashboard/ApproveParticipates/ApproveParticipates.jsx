import { useMutation } from "@tanstack/react-query";
import usePaymentCollection from "../../../Hooks/usePaymentCollection";
import useAxiosSecure from "../../../Hooks/useAxiosSecure";

const ApproveParticipates = () => {
    const axiosSecure = useAxiosSecure()
    const {paymentCollection,paymentCollectionLoading,paymentCollectionError,refetch} = usePaymentCollection()


    const {mutateAsync} = useMutation({
      mutationFn: async({userContestId,collection}) => {
        const {data} = await axiosSecure.patch(`/payment-status/${userContestId}`, {collection})
        return data
      },
    })
    
        console.log(paymentCollection);
        const handleStatusChange = (collection) =>{
          const userContestId = collection.userContestId;
          const paymentApproval = collection.paymentApproval
          console.log(collection);

          mutateAsync({userContestId,collection})
          .then((result) => {
            refetch()
          console.log(result);  
          }).catch((err) => {
            console.log(err);
          });
          
  }
  const accept=paymentCollection?.filter(d=>d.paymentApproval!=="Approve")
console.log(paymentCollection);
  return (
    <div>
   {
    accept?.length<=0 ? <>
    <><div className="h-screen flex items-center justify-center">
      <h1 className="text-purple-800 font-semibold text-2xl">No Acceptable Contest Found</h1>
      </div></>
    </>:
    <>
      {
    accept?.map((d,idx)=>(
        <div key={idx} className="collapse collapse-plus border border-blue-600 mb-2 bg-base-200 ">
        <input type="radio" name="my-accordion-3" defaultChecked />
        <div className="collapse-title  font-medium">
          <h1 className="text-xl">Contest Name : {d?.contestName}</h1>
          <p>Participate Name : {d?.name}</p>
        </div>
        <div className="collapse-content">
           <p>Transaction id : {d?.transactionId}</p>
          <p>Payment email address : {d?.email}</p>        
          <p>Payment Time : {d?.time},{d?.date}</p>        
          <p>Payment status: {d?.paymentApproval==="pending"?"Waiting for accept":'Approved'}</p> 
          {
            d?.paymentApproval==="Approve"?'':          <button onClick={()=>handleStatusChange(d)} className="btn btn-primary mt-5">Accept Payment</button>       

          }
          
          </div>
      </div>
      ))
     }
    </>
   }

    </div>
  );
};

export default ApproveParticipates;
