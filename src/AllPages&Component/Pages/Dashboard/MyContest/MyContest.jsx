import useAuth from "../../../Hooks/useAuth";
import { FaEdit } from "react-icons/fa";
import { TiDelete } from "react-icons/ti";


import { Link } from 'react-router-dom';
import useMyContest from "../../../Hooks/useMyContest";
import useAxiosSecure from "../../../Hooks/useAxiosSecure";
import { useState } from "react";
import { useMutation } from "@tanstack/react-query";
import toast from "react-hot-toast";
import Swal from "sweetalert2";

const MyContest = () => {
    const axiosSecure = useAxiosSecure()
    const [loading,setLoading] = useState(null)
    const {user} =  useAuth()
    const email = user?.email
    
    const {myContest , isLoading ,error,refetch} = useMyContest({email})
    console.log(myContest);

    const {mutateAsync:deleteContest}=useMutation({
      mutationFn:async(id)=>{
          const {data} = await axiosSecure.delete(`/contest/${id}`)
          return data
      },
      onSuccess:()=>{
          setLoading(false)
      },

      onError:(error)=>{
          setLoading(true)
          console.log(error);
          return toast.error(error.message)
      }
  })
  const handleDelete=(id)=>{
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then((result) => {
      if (result.isConfirmed) {
        deleteContest(id)
        setLoading(true)
        Swal.fire({
          title: "Deleted!",
          text: "Your file has been deleted.",
          icon: "success"
        });
      }
    });



}


  



refetch()
    return (
        <div className="font-poppins w-full mx-auto  ">
            <h1 className="font-bold text-2xl">Publish Contest : {myContest?.length} </h1>
            <div className="overflow-x-auto">
  <table className="table overflow-auto">
    {/* head */}
    <thead className="text-lg text-center">
        
      <tr>
        <th>Sl</th>
        <th>Contest Name</th>
        <th>Contest Price</th>
        <th>Publish Date</th>
        <th>Expire Date</th>
        <th>Total Participate</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      {/* row 1 */}
{
    myContest?.map((d,idx)=>(
        <tr key={idx} className="text-center text-sm">
        <th>{idx+1}</th>
        <td>{d.contestName}</td>
        <td>{d.price}</td>
        <td>{d.publishDate  }</td>
        <td>{d.endDate}</td>
        <td>{d.qty}</td>
        <td className="flex gap-2 text-xl justify-center" >
            <Link to={`contest-update/${d._id}`}><FaEdit className="cursor-pointer text-blue-800 " />
            </Link>
            <TiDelete  onClick={()=>handleDelete(d._id)} className="text-2xl cursor-pointer text-red-700"/>
        </td>
      </tr>
    ))
}
     
    </tbody>
  </table>

  <div>
    
  </div>
</div>


        </div>
    );
};

export default MyContest;