import { Link } from "react-router-dom";

const ErrorPage = () => {
    return (
        <div className="w-full h-screen bg-black flex flex-col justify-center items-center text-white text-3xl">
            <p>Oposss..</p> 
            <h1 className="text-4xl font-semibold text-purple-700">Something happen wrong</h1>
            <h1 className="text-4xl font-semibold text-purple-700">Please Back to </h1>
            <Link to={'/'}>
            <button className="bg-purple-500 ml-5 py-2 px-3 rounded-lg text-white mt-4 shadow-2xl border-white border-2 hover:bg-white hover:border-purple-600 hover:text-purple-500">
                Home
                </button> 
            </Link>
        </div>
    );
};

export default ErrorPage;