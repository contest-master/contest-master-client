import { Link, useLocation, useNavigate } from 'react-router-dom';
import useAuth from '../../Hooks/useAuth';
import toast from 'react-hot-toast';
import SocialLogin from '../../Components/SocialLogin/SocialLogin';
import { useState } from 'react';

const Login = () => {
  const {login,user} = useAuth()
  const location = useLocation()
  const path = location?.state?.from.pathname || '/'
  const [erros,setError] = useState(false)



  const navigate = useNavigate()

    const handleLogin=(e)=>{
        e.preventDefault()
        const field = e.target;
        const email =field.email.value;
        const password = field.password.value;

        login(email,password)
        .then(res=>{
            
              toast.success('Login Done')
              return navigate(path, {replace:true})
            
        })
        .catch(err=>{
          setError(true)
          return toast.error("Invalid email or password")
        })


        console.log(email,password);

    }

    return (
        <div>

        <div className="hero min-h-screen bg-[rgba(0,0,0,.5)]">
        
<div className="hero-content  ">

<div className="card shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
<h1 className='text-center my-3 text-xl font-poppins text-purple-700'>Please Login</h1>  
   
  <form className="card-body -mt-5" onSubmit={handleLogin}>

    <div className="form-control">
      <label className="label">
        <span className="label-text">Email</span>
      </label>
      <input type="email" placeholder="email" name="email" className="input input-bordered"  />
    </div>

    <div className="form-control">
      <label className="label">
        <span className="label-text">Password</span>
      </label>
      <input type="password" name='password' placeholder="password" className="input input-bordered" required />
      <label className="label">
        <a href="#" className="label-text-alt link link-hover">Forgot password?</a>
      </label>
      {
        erros && <><p className='text-red-500 '>Invalid email or password</p></>
      }
    </div>


    <div className="form-control  mt-3">
      <button className="btn btn-primary">Login</button>
    </div>
  </form>
    <div className='-mt-7'>
    <SocialLogin  />
    </div>
    <Link to='/signUp'><p className='p-3 '>Already have an account Please <span className='text-blue-700 font-bold'>SignUp</span></p></Link>
  <div>
  </div>
</div>
</div>
</div>
    </div>
    );
};

export default Login;