import {
    createBrowserRouter,
  } from "react-router-dom";
import HomeLayout from "../Layout/HomeLayout/HomeLayout";
import Home from "../Pages/Home/Home/Home";
import ErrorPage from "../Pages/ErrorPage/ErrorPage";
import SignUp from "../Pages/SignUp/SIgnUp.";
import Login from "../Pages/Login/Login";
import ContestSingleCard from "../Pages/Home/ContestSingleCard/ContestSingleCard";
import Payment from "../Pages/Home/Payment/Payment";
import DashboardLayout from "../Layout/DashboardLayout/DashboardLayout";
import UserProfile from "../Pages/Dashboard/UserProfle/UserProfile";
import AddContest from "../Pages/Dashboard/AddContest/AddContest";
import MyContest from "../Pages/Dashboard/MyContest/MyContest";
import MyContestUpdate from "../Pages/Dashboard/MyContestUpdate/MyContestUpdate";
import ParticipatedCart from "../Pages/Dashboard/ParticipatedCart/ParticipatedCart";
import ApproveParticipates from "../Pages/Dashboard/ApproveParticipates/ApproveParticipates";
import JoinContest from "../Pages/Dashboard/JoinContest/JoinContest";
import MyContestResult from "../Pages/Dashboard/MyContestResult/MyContestResult";
import SubmittedContest from "../Pages/Dashboard/SubmittedContest/SubmittedContest";
import AllContestPage from "../Pages/Home/AllContestPage/AllContestPage";
import AllUsers from "../Pages/Dashboard/AllUsers/AllUsers";
import PrivateRoute from "../Components/PrivateRoute/PrivateRoute";

const route = createBrowserRouter([
    {

        path:'/',
        element:<HomeLayout/>,
        errorElement:<ErrorPage/>,
        children:[
            {
                index:true,
                element:<Home/>
            },
            {
                path:'/contestDetails/:id',
                element:<PrivateRoute><ContestSingleCard/></PrivateRoute>
            },
            {
                path:'/payment/:id',
                element:<PrivateRoute><Payment/></PrivateRoute>
            },
            {
                path:'show-all',
                element:<AllContestPage/>
            }
        ]   
    }
    ,
    {
        path:'/signUp',
        element:<SignUp/>
    },
    {
        path:'/login',
        element:<Login/>
    },
    {
        path:'dashboard',
        element:<PrivateRoute><DashboardLayout/></PrivateRoute>,
        children:[
            {
                index:true,
                element: <UserProfile/>
            },
            {
                path:'add-contest',
                element:<PrivateRoute><AddContest/></PrivateRoute>
            },
            {
                path:'my-contest',
                element:<PrivateRoute><MyContest/></PrivateRoute>
            },
            {
                path:'my-contest/contest-update/:id',
                element:<PrivateRoute><MyContestUpdate/></PrivateRoute>
            }
            ,
            {
                path:'approve-participates',
                element:<PrivateRoute><ApproveParticipates/></PrivateRoute>
            },
            {
                path:'cart',
                element:<PrivateRoute><ParticipatedCart/></PrivateRoute>
            },
            {
                path:'join-contest',
                element:<PrivateRoute><JoinContest/></PrivateRoute>
            },
            {
                path:'my-contest-result',
                element:<PrivateRoute><MyContestResult/></PrivateRoute>
            },
            {
                path:'submitted-contest',
                element:<PrivateRoute><SubmittedContest/></PrivateRoute>
            },
                
            {
                path:'all-users',
                element:<PrivateRoute><AllUsers/></PrivateRoute>
            }
          ,
        ]
    }
]);


export default route
