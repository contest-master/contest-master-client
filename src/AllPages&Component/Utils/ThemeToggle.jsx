import React, { useEffect, useState } from 'react';

const ThemeToggle = () => {
    const [theme,setTheme] = useState(localStorage.getItem('theme')? 'light':('synthwave'))

    useEffect(()=>{
        localStorage.setItem('theme',theme)
        const local = localStorage.getItem('theme')
        document.querySelector('html').setAttribute('data-theme',local)
    },[theme])


    const handleChange=(e)=>{
        const currentTheme=e.target.checked
        if(currentTheme){
            setTheme('synthwave')   
        }else{
            setTheme('light')
        }
    }
    return (
        <div>
            <input onChange={(e)=>handleChange(e)} type="checkbox" value="synthwave" className="toggle theme-controller"/>
        </div>
    );
};

export default ThemeToggle; 