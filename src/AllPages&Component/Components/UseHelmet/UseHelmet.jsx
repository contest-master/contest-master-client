import { Helmet } from "react-helmet";
import PropTypes from 'prop-types';

const UseHelmet = ({title}) => {
    return (
        <div>
           <Helmet><title>Contest Master | {title}</title></Helmet> 
        </div>
    );
};

UseHelmet.propTypes = {
    title: PropTypes.string.isRequired,
};

export default UseHelmet;