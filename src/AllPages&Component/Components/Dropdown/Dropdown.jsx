import { useState } from "react";
import "./dropwon.css";
import useDbUserData from "../../Hooks/useDbUserData";
import useAxiosSecure from "../../Hooks/useAxiosSecure";
import { useMutation } from "@tanstack/react-query";
import toast from "react-hot-toast";
import Swal from "sweetalert2";

const Dropdown = ({ userId }) => {
  const [isShow, setShow] = useState(false);
  const { dbUser ,dbUserLoading,dbUserRefetch} = useDbUserData();
  const currentUser = dbUser?.find((d) => d._id === userId);
  const axiosSecure = useAxiosSecure();

  const selectedUserRole = currentUser.role;
  const email = currentUser.email

  const { mutateAsync:update } = useMutation({
    
  mutationFn:async (data) => {
    await axiosSecure.patch("/user-role-update",data);
    }
    ,
    onSuccess: () => {
        dbUserRefetch()
        toast.success("User Role Updated");
      },
      onError: () => {
        toast.error("Update Failed");
      }
  });




  const { mutateAsync:deleted } = useMutation({
    
    mutationFn:async (email) => {
      await axiosSecure.delete(`/user-delete?email=${email}` );
      }
      ,
      onSuccess: () => {
          dbUserRefetch()
          toast.success("User Deleted Successfully");
        },
        onError: () => {
          toast.error("Update Failed");
        }
    });




  const handleOnClick = (value) => {
    const role = value;
    const data = { email , role}
    console.log(data);
    try {
        update(data)
    } catch (error) {
        console.log(error);
    }
    console.log(role);
  };




  const handleOnDelete =async () => {
    Swal.fire({
        title: "Are you sure?",
        text: ` Deleted  ${email} ?`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
      }).then((result) => {
        if (result.isConfirmed) {
        
            deleted(email)
        }
      });


        console.log(email);
  };

  return (
    <div>
      <div className="relative">
        <button onClick={() => setShow(!isShow)} className="button-30">
          Manage
        </button>

        {isShow && (
          <div className="parent-div absolute z-50 md:ml-12">
            {selectedUserRole === "Admin" && (
              <>
                <button
                  onClick={() => handleOnClick("Moderator")}
                  className="button-29"
                >
                  Make Moderator
                </button>
                <button
                  onClick={() => handleOnClick("User")}
                  className="button-31"
                >
                  Make user
                </button>
                <button
                  onClick={() => handleOnDelete("")}
                  className="button-32"
                >
                  Delete User
                </button>
              </>
            )}

            {selectedUserRole === "User" && (
              <>
                <button
                  onClick={() => handleOnClick("Moderator")}
                  className="button-29"
                >
                  Make Moderator
                </button>
                <button
                  onClick={() => handleOnDelete("")}
                  className="button-32"
                >
                  Delete User
                </button>
                <button
                  onClick={() => handleOnClick("Admin")}
                  className="button-30"
                >
                  Make Admin
                </button>
              </>
            )}

            {selectedUserRole === "Moderator" && (
              <>
                <button
                  onClick={() => handleOnClick("User")}
                  className="button-31"
                >
                     <button
                  onClick={() => handleOnClick("Moderator")}
                  className="button-31"
                ></button>
                  Make user
                </button>
                <button
                  onClick={(email) => handleOnDelete(email)}
                  className="button-32"
                >
                  Delete User
                </button>
             
              </>
            )}
            
          </div>
        )}
      </div>
    </div>
  );
};

export default Dropdown;
