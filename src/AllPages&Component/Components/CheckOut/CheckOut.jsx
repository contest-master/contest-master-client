import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import { useEffect, useState } from "react";
import useUserParticipatedData from "../../Hooks/useUserParticipatedData";
import { useNavigate, useParams } from "react-router-dom";
import useAxiosSecure from "../../Hooks/useAxiosSecure";
import useAuth from "../../Hooks/useAuth";
import toast from "react-hot-toast";

const CheckOut = () => {
    const [errors,setErrors]=useState()
    const {id} = useParams()
    const {participatedData} = useUserParticipatedData()
    
    const contest = participatedData?.find(d=>d.contestId===id)

    const price = contest?.contestPrice
    const contestId = contest?.contestId
    const contestName = contest?.contestName
    const publisher = contest?.publisher
    const userContestId = contest?._id
    const axiosSecure = useAxiosSecure()
    const {user} = useAuth()
    const [clientSecret,setClientSecret] = useState()
    const [transactionId,setTransactionId] = useState()
    const navigate = useNavigate()

    console.log(contest);





    useEffect(()=>{
     if(price>0){
      axiosSecure.post(`/create-payment-intent`,{price})
      .then((result) => {
        console.log(result.data.clientSecret);
        setClientSecret(result.data.clientSecret)
      }).catch((err) => {
        console.log(err);
      });
     }
    },[price,axiosSecure])


    const stripe = useStripe()
    const elements = useElements()

    const handleOnSubmit=async(e)=>{
        e.preventDefault()

        if(!stripe || !elements){
            return
        }

        const card = elements.getElement(CardElement)
        if(card===null){
            return
        }

        const {error,paymentMethod} = await stripe.createPaymentMethod({
            type:'card',
            card    
        })

        if(error){
            console.log('stipe error',errors);
                setErrors(error)
        }
        else{
            console.log('paymentMethod', paymentMethod) ;
        }

        try {
          const {paymentIntent, error:confirmError} = await stripe.confirmCardPayment(clientSecret,{
          payment_method:{
          card:card,
          billing_details:{
          email : user?.email,
          name:user?.displayName || 'anonymous',
          },
          metadata:{
           contestId:contestId
          }
          }
          });
          if (confirmError) {
            console.error('confirmError---->', confirmError);
        } else {
            console.log('paymentIntent--->', paymentIntent.id);
        }

        if(paymentIntent?.status==="succeeded"){
          setTransactionId(paymentIntent?.id)
          const paymentCollection={
              email:user.email,
              name:user.displayName,
              userContestId,
              contestId,
              contestName,
              transactionId:paymentIntent.id,
              amount:price,
              time:new Date().toLocaleTimeString(),
              date:new Date().toDateString(),
              paymentApproval:"pending",
              publisher
          }

          await axiosSecure.post('/payment',paymentCollection)
          .then((res)=>{
            if(res.data.insertedId){
              if(res?.data?.insertedId)
                return navigate(-1)
              return toast.success('Payment Done Waiting for confirmation')
            }
            console.log(res);
          })
          .catch((error)=>{
            console.log(error);
          })

        }

       } catch (error) {
         console.log(error);
       }
     

    }

    return (
        <div className="w-full mx-auto h-screen flex items-center justify-center     ">
          
            <form onSubmit={handleOnSubmit} className="bg-purple-800 rounded-lg">
                <div className="w-80 p-3 mx-auto border-yellow-400 border-4 rounded-lg ">
                <CardElement className="w-50 border-2 border-white py-3 px-3 my-2 rounded-2xl bg-white" 
        options={{
          style: {
            base: {
              fontSize: '16px',
              color: '#424770',
              '::placeholder': {
                color: '#aab7c4',
              },
            },
            invalid: {
              color: '#9e2146',
            },
          },
        }}
      />
      {transactionId?<><p className="text-sm">Your transaction Id :{transactionId}</p></>:''}
   
      <button className="btn-sm bg-purple-600 px-7 rounded-lg border-2 border-white text-white" type="submit" disabled={ !clientSecret ||    !stripe}>
        Pay
      </button>
                </div>
            </form>
        </div>
    );
};

export default CheckOut;