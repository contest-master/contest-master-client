import {category} from '../../Components/ContestForm/category'
import { DateRange } from 'react-date-range';
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file
import TitleAndSubtitle from '../utils/TitleAndSubtitle';


export const ContestForm = ({handleSubmit , dates , handleDate,setImageFile}) => {
    return (
        <div className='-mt-24'>
            <TitleAndSubtitle title={"Add Contest"} subTitle={"Before Publish check again everything is ok or not"} />
            <div className='border-2 md:w-4/5 p-2 mx-auto border-purple-500 mt-5 text-center '>
                <form onSubmit={handleSubmit} className=''>

                <div className='flex flex-col md:flex-row mx-2' >
                        
                        <div className='w-full'>
                        <input type="text" name='contestName' placeholder="Contest Name" className="input input-bordered input-error w-full max-w-xs" />
                        <DateRange
                    
                    rangeColors={["#F43F5e"]}
                    editableDateInputs={true}
                    onChange={item => handleDate(item)}
                    moveRangeOnFirstSelection={false}
                    ranges={[dates]}  
                />

                        </div>


                        <div className='w-full'>
                        <input type="number" name='contestPrice' placeholder="Contest Price" className="input input-bordered input-error w-full my-2 max-w-xs" />

                        <select name='contestType' className="select my-2 select-primary input input-bordered input-error w-full max-w-xs ">
                            {
                            category.map((d,idx)=>(<option key={idx} value={d.contestType} > {d.contestType} </option>))
                            } 
                        </select>

                        <input type="text" name='prize'  placeholder="Wining Prize" className="input input-bordered my-2 input-error w-full max-w-xs" />
                        <input type="text" name='contestDescription' placeholder="Description" className="input input-bordered my-2 input-error  w-full  max-w-xs" />
                        <input type="text" name='taskSubmissionInstructions' placeholder="Task Instruction" className="input my-2 input-bordered input-error w-full max-w-xs" />
                        <div className='text-left ml-16'>

                        <input className='my-2' onChange={()=>setImageFile(event.target.files[0])} type='file' name='image'/>
                </div>

                <button className='btn btn-primary'>Submit</button>

                        

                        </div>
                        

                </div>


                </form>
            </div>
        </div>
    );
};

