import { Navigate, useLocation } from "react-router-dom";
import useAuth from "../../Hooks/useAuth";
import LoadingComponent from "../LoadingComponent/LoadingComponent";

const PrivateRoute = ({children}) => {
    const location = useLocation()
    
    const {user,loading} = useAuth()
    
    if(loading){
        return <LoadingComponent/>
    }

    if(user){
        return children
    }

    return <Navigate to='/login' state={{from:location}} replace />
};

export default PrivateRoute;