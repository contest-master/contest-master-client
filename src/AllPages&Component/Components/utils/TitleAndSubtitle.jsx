
const TitleAndSubtitle = ({title,subTitle}) => {

    return (
        <div className="font-poppins text-center mt-24 mb-5">
            <h1 className="headerText   mx-[3px] text-4xl md:text-5xl">{title}</h1>
            <p className="hidden md:block headerText text-lg md:text-xl my-3 mb-9">----------{subTitle}----------</p>
            <p className="headerText md:hidden text-lg mx-7 my-3 mb-9">{subTitle}</p>
        </div>
    );
};

export default TitleAndSubtitle;