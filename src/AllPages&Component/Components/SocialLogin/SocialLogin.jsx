import { useNavigate } from "react-router-dom";
import useAuth from "../../Hooks/useAuth";
import GoogleButton from "react-google-button";
import toast from "react-hot-toast";
import useAxiosSecure from "../../Hooks/useAxiosSecure";

const SocialLogin = () => {
  const { googleLogin } = useAuth();
  const navigate = useNavigate()
  const axiosSecure= useAxiosSecure()
  const handleLogin=()=>{
    googleLogin()
    .then(async({user})=>{
      navigate('/') 
      console.log(user)
      const email = user.email
      const name = user.displayName
      const imageUrl = user.photoURL
      const role = 'User'
      const userProfile ={
        email,name,imageUrl,role
    }

        const data =await axiosSecure.post('/user-data',userProfile)
        console.log(data);
        toast.success('Login Done')
        
    })
  }
  return (
    <div className="w-full text-center flex justify-center mx-auto">
      <GoogleButton 
        onClick={handleLogin}
      />
    </div>
  );
};

export default SocialLogin;
