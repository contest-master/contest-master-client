
const LoadingComponent = () => {
    return (
        <div className='w-full h-screen  '> 
            <div className='flex items-center justify-center'>
                <span className="loading loading-bars purpleText loading-xs"></span>
            </div>
        </div>
    );
};

export default LoadingComponent;